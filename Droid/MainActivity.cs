﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;
using System.Linq;

namespace BlackjackSimulator.Droid
{
	[Activity(Label = "Blackjack Simulator", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Main);
			WireUpGui();
		}

		void SimulationButton_Click(object sender, EventArgs e)
		{
			var resultsIntent = new Intent(this, typeof(SimulationResults));
			int parsedValue = -1;

			int.TryParse(FindViewById<Spinner>(Resource.Id.spinner1).SelectedItem.ToString(), out parsedValue);
			resultsIntent.PutExtra("decks", parsedValue);
			int.TryParse(FindViewById<Spinner>(Resource.Id.spinner2).SelectedItem.ToString(), out parsedValue);
			resultsIntent.PutExtra("players", parsedValue);
			int.TryParse(FindViewById<Spinner>(Resource.Id.spinner3).SelectedItem.ToString(), out parsedValue);
			resultsIntent.PutExtra("rounds", parsedValue);
			int.TryParse(FindViewById<Spinner>(Resource.Id.spinner4).SelectedItem.ToString(), out parsedValue);
			resultsIntent.PutExtra("cash", parsedValue);
			int.TryParse(FindViewById<Spinner>(Resource.Id.spinner5).SelectedItem.ToString(), out parsedValue);
			resultsIntent.PutExtra("bet", parsedValue);
			resultsIntent.PutExtra("betstrat", FindViewById<Spinner>(Resource.Id.spinner6).SelectedItem.ToString());
			StartActivity(resultsIntent);
		}

		void WireUpGui()
		{
			Button button = FindViewById<Button>(Resource.Id.myButton);
			button.Click += SimulationButton_Click;

			Spinner decks = FindViewById<Spinner>(Resource.Id.spinner1);
			var deckAdapt = new ArrayAdapter<int>(this, Android.Resource.Layout.SimpleSpinnerItem, new int[]{1,2,3,4});
			deckAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			decks.Adapter = deckAdapt;

			Spinner players = FindViewById<Spinner>(Resource.Id.spinner2);
			var playerAdapt = new ArrayAdapter<int>(
				this, Android.Resource.Layout.SimpleSpinnerItem, new int[] { 1, 2, 3, 4 });
			playerAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			players.Adapter = playerAdapt;

			Spinner rounds = FindViewById<Spinner>(Resource.Id.spinner3);
			var roundAdapt = new ArrayAdapter<int>(
				this, Android.Resource.Layout.SimpleSpinnerItem, new int[] { 1, 5, 10, 50, 100, 500, 1000, 50000 });
			roundAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			rounds.Adapter = roundAdapt;

			Spinner cash = FindViewById<Spinner>(Resource.Id.spinner4);
			var cashAdapt = new ArrayAdapter<int>(
				this, Android.Resource.Layout.SimpleSpinnerItem, new int[] {  100, 500, 1000, 50000 });
			cashAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			cash.Adapter = cashAdapt;

			Spinner bet = FindViewById<Spinner>(Resource.Id.spinner5);
			var betAdapt = new ArrayAdapter<int>(
				this, Android.Resource.Layout.SimpleSpinnerItem, new int[] { 5, 10, 20, 25, 50, 75, 100 });
			betAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			bet.Adapter = betAdapt;

			Spinner strat = FindViewById<Spinner>(Resource.Id.spinner6);
			var stratAdapt = new ArrayAdapter<string>(
				this, Android.Resource.Layout.SimpleSpinnerItem, new string[] { "Boring", "Progressive"});
			stratAdapt.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			strat.Adapter = stratAdapt;
		}
}
}

