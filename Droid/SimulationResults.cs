﻿using System;

using Android.App;
using Android.OS;
using Android.Text.Method;
using Android.Widget;

namespace BlackjackSimulator.Droid
{
	[Activity(Label = "SimulationResults")]
	public class SimulationResults : Activity
	{
		int numOfDecks = -1, numOfPlayers = -1, numOfRounds = -1, startingCash = -1, betAmt = -1;
		string bettingStrat = "";
		SimulatorWrapper sim = null;

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Results);

			WireUpGui();

			numOfDecks = Intent.GetIntExtra("decks", -1);
			numOfPlayers = Intent.GetIntExtra("players", -1);
			numOfRounds = Intent.GetIntExtra("rounds", -1);

			startingCash = Intent.GetIntExtra("cash", -1);
			betAmt = Intent.GetIntExtra("bet", -1);
			bettingStrat = Intent.GetStringExtra("betstrat");
		}

		protected override void OnResume()
		{
			base.OnResume();
			SimulateRounds(numOfRounds);
		}

		void ReSimulate(object sender, EventArgs e)
		{
			SimulateRounds(numOfRounds);
		}

		void SimulateRounds(int numberOfRounds)
		{
			var strat = bettingStrat.ToLower() == "boring" ? 0 : bettingStrat.ToLower() == "progressive" ? 1 : 0;
			sim = new SimulatorWrapper(numOfDecks, numOfPlayers, startingCash, betAmt, strat);
			sim.SimulateRounds(numberOfRounds);

			var txtViewOutput = FindViewById<TextView>(Resource.Id.textView1);
			txtViewOutput.Text = sim.GetOutput();

			var txtViewResults = FindViewById<TextView>(Resource.Id.textView2);
			string results = $"Results\r\nStarting cash: ${sim.StartingCash}\r\n";
			var players = sim.GetPlayers();

			foreach (var player in players)
			{
				results += $"{player.Name}'s Cash: ${player.cash}\r\n";
			}

			txtViewResults.Text = results + "\r\n";
		}

		void ShowOutput(object sender, EventArgs e)
		{
			var txtView = FindViewById<TextView>(Resource.Id.textView1);
			txtView.Visibility = txtView.Visibility == Android.Views.ViewStates.Visible 
				? Android.Views.ViewStates.Invisible
				: Android.Views.ViewStates.Visible;
		}

		void WireUpGui()
		{
			var button = FindViewById<Button>(Resource.Id.button1);
			button.Click += ReSimulate;

			var btnShowOutput = FindViewById<Button>(Resource.Id.toggleButton1);
			btnShowOutput.Click += ShowOutput;

			//Allow scrolling on text views
			var txtView = FindViewById<TextView>(Resource.Id.textView1);
			var txtViewResults = FindViewById<TextView>(Resource.Id.textView2);
			txtViewResults.MovementMethod = new ScrollingMovementMethod();
			txtView.Visibility = Android.Views.ViewStates.Invisible;
			txtView.MovementMethod = new ScrollingMovementMethod();
		}

}
}
