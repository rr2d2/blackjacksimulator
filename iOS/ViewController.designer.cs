// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace BlackjackSimulator.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView BetPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView BetStratPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView CashPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView DecksPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView PlayersPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView RoundsPicker { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (BetPicker != null) {
                BetPicker.Dispose ();
                BetPicker = null;
            }

            if (BetStratPicker != null) {
                BetStratPicker.Dispose ();
                BetStratPicker = null;
            }

            if (Button != null) {
                Button.Dispose ();
                Button = null;
            }

            if (CashPicker != null) {
                CashPicker.Dispose ();
                CashPicker = null;
            }

            if (DecksPicker != null) {
                DecksPicker.Dispose ();
                DecksPicker = null;
            }

            if (PlayersPicker != null) {
                PlayersPicker.Dispose ();
                PlayersPicker = null;
            }

            if (RoundsPicker != null) {
                RoundsPicker.Dispose ();
                RoundsPicker = null;
            }
        }
    }
}