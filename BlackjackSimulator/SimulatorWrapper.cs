﻿using System;
using System.Collections.Generic;
using Jackal;
namespace BlackjackSimulator
{
	public class SimulatorWrapper
	{
		public int NumberOfDecks { get; set; }
		public int NumberOfPlayers { get; set; }
		public int StartingCash { get; set; }
		public int BettingAmt { get; set; }

		BlackJackSim _instance = null;

		public SimulatorWrapper(int numOFDecks = 4, int numOfPlayers = 2, 
		                        int startingAmt = 100, int betAmt = 5, int bettingStratedy = 0)
		{
			NumberOfDecks = numOFDecks;
			NumberOfPlayers = numOfPlayers;
			StartingCash = startingAmt;
			BettingAmt = betAmt;

			_instance = new BlackJackSim(numOFDecks, numOfPlayers, startingAmt, 
			                             betAmt, GetStrat(bettingStratedy));
		}

		public void SimulateRounds(int rounds = 1)
		{
			_instance.SimulateRounds(rounds);
		}

		public List<Player> GetPlayers()
		{
			return _instance.Players;
		}

		public string GetOutput()
		{
			var output = CustomLogger.GetLogString();
			CustomLogger.ClearLog();
			return output;
		}

		//Poop here
		IBettingStrategy GetStrat(int strat)
		{
			switch (strat)
			{
				case 1:
					return new ProgressiveBetting();
				default:
					return new BoringBetting();
			}
		}
	}
}
